package com.example.myapplication.Network.model.branch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ahmed Hateem on 11/27/2017.
 */

public class Branch implements Serializable {

    @SerializedName("Id")
    @Expose
    private Long id;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("BreifAR")
    @Expose
    private Object breifAR;
    @SerializedName("BreifEN")
    @Expose
    private Object breifEN;
    @SerializedName("Fax")
    @Expose
    private Object fax;
    @SerializedName("Tele")
    @Expose
    private String tele;
    @SerializedName("X_Piont")
    @Expose
    private String lat;
    @SerializedName("Y_Piont")
    @Expose
    private String lng;
    @SerializedName("CountryId")
    @Expose
    private Long countryId;
    @SerializedName("CountryArabicName")
    @Expose
    private String countryArabicName;
    @SerializedName("CountryEnglishName")
    @Expose
    private String countryEnglishName;
    @SerializedName("CityId")
    @Expose
    private Long cityId;
    @SerializedName("CityArabicName")
    @Expose
    private String cityArabicName;
    @SerializedName("CityEnglishName")
    @Expose
    private String cityEnglishName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArabicName() {
        if (arabicName != null) {
            return arabicName;
        } else return "";
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {

        if (englishName != null) {
            return englishName;
        } else return "";
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getAddress() {

        if (address != null) {
            return address;
        } else return "";
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getBreifAR() {
        return breifAR;
    }

    public void setBreifAR(Object breifAR) {
        this.breifAR = breifAR;
    }

    public Object getBreifEN() {
        return breifEN;
    }

    public void setBreifEN(Object breifEN) {
        this.breifEN = breifEN;
    }

    public Object getFax() {
        return fax;
    }

    public void setFax(Object fax) {
        this.fax = fax;
    }

    public String getTele() {
        return tele;
    }

    public void setTele(String tele) {
        this.tele = tele;
    }

    public Double getLat() {

        if (lat == null) {
            lat = String.valueOf(0.0);
        }
        return Double.valueOf(lat);

    }

    public void setLat(String xPiont) {
        this.lat = xPiont;
    }

    public Double getLng() {
        if (!lng.equals(null)) {
            return Double.parseDouble(lng);
        } else return 0.0;

    }

    public void setLng(String yPiont) {
        this.lng = yPiont;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getCountryArabicName() {

        if (countryArabicName == null) {
            countryArabicName = "";
        }
        return countryArabicName;
    }

    public void setCountryArabicName(String countryArabicName) {
        this.countryArabicName = countryArabicName;
    }

    public String getCountryEnglishName() {
        if (countryEnglishName == null) {
            countryEnglishName = "";
        }
        return countryEnglishName;
    }

    public void setCountryEnglishName(String countryEnglishName) {
        this.countryEnglishName = countryEnglishName;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getCityArabicName() {

        if (cityArabicName != null) {
            return cityArabicName;
        } else return "";

    }

    public void setCityArabicName(String cityArabicName) {
        this.cityArabicName = cityArabicName;
    }

    public String getCityEnglishName() {
        if (cityEnglishName == null) {
            cityEnglishName = "";
        }
        return cityEnglishName;
    }

    public void setCityEnglishName(String cityEnglishName) {
        this.cityEnglishName = cityEnglishName;
    }
}
