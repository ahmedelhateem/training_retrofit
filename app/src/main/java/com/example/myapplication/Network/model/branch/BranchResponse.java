package com.example.myapplication.Network.model.branch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Created by Ahmed Hateem on 11/27/2017.
 */

public class BranchResponse implements Serializable {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("BranchesLst")
    @Expose
    private List<Branch> branchesList = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Branch> getBranchesList() {
        if (branchesList == null) {
            branchesList = Collections.emptyList();
        }
        return branchesList;
    }

    public void setBranchesList(List<Branch> branchesList) {
        this.branchesList = branchesList;
    }
}
