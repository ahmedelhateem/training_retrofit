package com.example.myapplication.Network.model.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CategoryList {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("CategoryImage")
    @Expose
    private String categoryImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArabicName() {
        if (arabicName == null) {
            arabicName = "";
        }
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        if (englishName == null) {
            englishName = "";
        }
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getCategoryImage() {
        if (categoryImage == null) {
            categoryImage = "";
        }
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

}
