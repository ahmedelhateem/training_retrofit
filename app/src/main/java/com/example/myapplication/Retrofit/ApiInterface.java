package com.example.myapplication.Retrofit;


import com.example.myapplication.Network.model.branch.BranchResponse;
import com.example.myapplication.Network.model.category.CategoriesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET("GetCategories")
    Call<CategoriesResponse> getCategories();

    @POST("GetBranches")
    Call<BranchResponse> getBranches();


}

