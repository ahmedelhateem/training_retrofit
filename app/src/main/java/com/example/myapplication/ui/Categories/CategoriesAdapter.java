package com.example.myapplication.ui.Categories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Network.model.category.CategoryList;
import com.example.myapplication.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryItemView> {


    private Context context;
    private List<CategoryList> categoryList = new ArrayList<>();

    public CategoriesAdapter(Context context) {
        this.context = context;

    }

    public void setCategoryList(List<CategoryList> categoryList) {
        if (categoryList == null) {
            return;
        }

        this.categoryList = categoryList;
    }

    @Override
    public CategoryItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_category, parent, false);
        return new CategoryItemView(view);
    }

    @Override
    public void onBindViewHolder(final CategoryItemView holder, int position) {
        final CategoryList category = categoryList.get(position);

        holder.productNameTV.setText(category.getArabicName());


        holder.progressBar.setVisibility(View.VISIBLE);
        holder.progressBar.setIndeterminate(true);


        holder.progressBar.setVisibility(View.GONE);

        Picasso.get()
                .load(category.getCategoryImage())
                .placeholder(context.getResources().getDrawable(R.drawable.ic_launcher_background))
                .resize(512, 512)
                .into(holder.categoryIV, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        holder.progressBar.setVisibility(View.GONE);
                    }


                });

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    class CategoryItemView extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView productNameTV;
        ImageView categoryIV;
        ProgressBar progressBar;

        CategoryItemView(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.Card_items);
            productNameTV = itemView.findViewById(R.id.TV_product);
            categoryIV = itemView.findViewById(R.id.IV_category);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }
    }
}
