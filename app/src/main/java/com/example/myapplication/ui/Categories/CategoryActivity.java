package com.example.myapplication.ui.Categories;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Network.model.category.CategoriesResponse;
import com.example.myapplication.R;

import java.util.List;

public class CategoryActivity extends AppCompatActivity implements CategoryListener {
    CategoriesAdapter categoriesAdapter;
    VideoAdapter videoAdapter;
    RecyclerView recyclerView;
    CategoryPresenter categoryPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main22);
        recyclerView = findViewById(R.id.RV_products);
        categoryPresenter = new CategoryPresenter(this, this);
        initiRecycleView();
        categoryPresenter.getVideos();

    }

    private void initiRecycleView() {
        categoriesAdapter = new CategoriesAdapter(this);
        videoAdapter = new VideoAdapter(this);

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(videoAdapter);
    }

    @Override
    public void setCategories(CategoriesResponse categoriesResponse) {
        categoriesAdapter.setCategoryList(categoriesResponse.getCategoryList());
        categoriesAdapter.notifyDataSetChanged();
    }

    @Override
    public void setError(String message) {

    }

    @Override
    public void setVideos(List<String> videos) {
        videoAdapter.setCategoryList(videos);
    }
}