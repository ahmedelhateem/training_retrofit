package com.example.myapplication.ui.Categories;


import com.example.myapplication.Network.model.category.CategoriesResponse;

import java.util.List;

public interface CategoryListener {

    void setCategories(CategoriesResponse categoriesResponse);

    void setError(String message);

    void setVideos(List<String> videos);
}
