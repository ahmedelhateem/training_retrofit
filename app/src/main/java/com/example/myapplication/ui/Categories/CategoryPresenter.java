package com.example.myapplication.ui.Categories;

import android.content.Context;

import com.example.myapplication.Network.model.category.CategoriesResponse;
import com.example.myapplication.Retrofit.ApiClient;
import com.example.myapplication.Retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryPresenter {
    Context context;
    CategoryListener categoryListener;

    public CategoryPresenter(Context context, CategoryListener categoryListener) {
        this.context = context;
        this.categoryListener = categoryListener;
    }

    public void getCategories() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CategoriesResponse> call = apiInterface.getCategories();
        call.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                if (response.isSuccessful()) {
                    String status = response.body().getSuccess();
                    if (status.equals("ok")) {
                        categoryListener.setCategories(response.body());
                    }

                } else {
                    categoryListener.setError("");
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                categoryListener.setError("");
            }
        });
    }

    public void getVideos() {
        final List<String> list = new ArrayList<>();
        list.add("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        list.add("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        list.add("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        list.add("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        list.add("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        list.add("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        categoryListener.setVideos(list);
    }
}
