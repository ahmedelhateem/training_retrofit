package com.example.myapplication.ui.Categories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.ui.test.Contacts;

import java.util.ArrayList;
import java.util.List;


public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.CategoryItemView> {


    private Context context;
    private List<Contacts> categoryList = new ArrayList<>();

    public ContactsAdapter(Context context) {
        this.context = context;

    }

    public void setCategoryList(List<Contacts> categoryList) {
        if (categoryList == null) {
            return;
        }

        this.categoryList = categoryList;
        notifyDataSetChanged();
    }

    @Override
    public CategoryItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_item, parent, false);
        return new CategoryItemView(view);
    }

    @Override
    public void onBindViewHolder(final CategoryItemView holder, int position) {
        final Contacts category = categoryList.get(position);

        holder.name.setText(category.getName());

        holder.number.setText(category.getNumber());

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    class CategoryItemView extends RecyclerView.ViewHolder {

        TextView name, number;


        CategoryItemView(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.number);

        }
    }
}
