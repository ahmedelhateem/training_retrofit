package com.example.myapplication.ui.Categories;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.List;


public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.CategoryItemView> {


    private Context context;
    private List<String> categoryList = new ArrayList<>();

    public VideoAdapter(Context context) {
        this.context = context;

    }

    public void setCategoryList(List<String> categoryList) {
        if (categoryList == null) {
            return;
        }

        this.categoryList = categoryList;
    }

    @Override
    public CategoryItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_video, parent, false);
        return new CategoryItemView(view);
    }

    @Override
    public void onBindViewHolder(final CategoryItemView holder, int position) {
        final String category = categoryList.get(position);

        final MediaController mediacontroller = new MediaController(context);
        mediacontroller.setAnchorView(holder.videoView);


        holder.videoView.setMediaController(mediacontroller);
        holder.videoView.setVideoURI(Uri.parse(category));
        holder.videoView.requestFocus();

        holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        holder.videoView.setMediaController(mediacontroller);
                        mediacontroller.setAnchorView(holder.videoView);

                    }
                });
            }
        });

        holder.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(context, "Video over", Toast.LENGTH_SHORT).show();

                holder.videoView.setVideoURI(Uri.parse(category));
                holder.videoView.start();


            }
        });

        holder.videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("API123", "What " + what + " extra " + extra);
                return false;
            }
        });
    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    class CategoryItemView extends RecyclerView.ViewHolder {
        VideoView videoView;

        //  Button play,pause;
        CategoryItemView(View itemView) {
            super(itemView);
            videoView = itemView.findViewById(R.id.videoView);
            // play=itemView.findViewById(R.id.play);
            // pause=itemView.findViewById(R.id.pause);

        }
    }
}
