package com.example.myapplication.ui.map;

import com.example.myapplication.Network.model.branch.Branch;

import java.util.List;


/**
 * Created by AMR on 11/27/2017.
 */

public interface BranchInterface {

    void getBranch(List<Branch> branchesList);

    void getBranchFailure(String message);
}
