package com.example.myapplication.ui.map;

import com.example.myapplication.Network.model.branch.BranchResponse;
import com.example.myapplication.Retrofit.ApiClient;
import com.example.myapplication.Retrofit.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Ahmed Hateem on 11/27/2017.
 */

public class BranchPresenter {

    private BranchInterface branchInterface;


    public BranchPresenter(BranchInterface branchInterface) {
        this.branchInterface = branchInterface;
    }

    public void getBranches() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BranchResponse> call = apiInterface.getBranches();
        //  Log.e("getBranches**", call.request().toString());
        call.enqueue(new Callback<BranchResponse>() {

            @Override
            public void onResponse(Call<BranchResponse> call, Response<BranchResponse> response) {
                if (response.isSuccessful()) {
                    branchInterface.getBranch(response.body().getBranchesList());
                    // Log.e("getBranches**", call.request().toString());

                } else {
                    branchInterface.getBranchFailure("fail");
                }
            }

            @Override
            public void onFailure(Call<BranchResponse> call, Throwable t) {
                branchInterface.getBranchFailure("fail");

            }
        });
    }

}

