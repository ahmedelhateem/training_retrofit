package com.example.myapplication.ui.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.myapplication.Network.model.branch.Branch;
import com.example.myapplication.R;
import com.example.myapplication.services.networkCheck;
import com.example.myapplication.services.LocationService;
import com.example.myapplication.utils.GpsProviderHelper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;


import java.util.List;
import java.util.Locale;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks,
        LocationSource.OnLocationChangedListener,
        LocationListener, GoogleMap.OnMapClickListener, GoogleMap.OnMapLoadedCallback, BranchInterface {

    private static final int LOCATION_SERVICE_CODE = 500;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 15;
    networkCheck broadCastReceiver;
    IntentFilter intentFilter;
    private LocationManager locationManager;
    private GoogleApiClient googleApiClient;
    private Bitmap locationIcon, branchIcon, branchSelectedIcon;
    private Location userCurrentLocation;
    private LatLng userLocation, branchLocation;
    private MarkerOptions currentUserMarker, branchMarker;
    private PopupWindow markerPopupWindow;
    private RelativeLayout anchorRL;
    private Marker mMarkerMyLocation = null, mPreMarker = null;
    private BranchPresenter branchPresenter;
    private AppCompatTextView branchAddress;
    private AppCompatTextView branchDistance, branchDistric;
    private String[] branchData;
    private LinearLayoutCompat goBranch, mapDialog;
    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;
    private AppCompatButton startService, stopService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        branchIcon = BitmapFactory.decodeResource(getResources(), R.drawable.check_bra);
        branchSelectedIcon = BitmapFactory.decodeResource(getResources(), R.drawable.map_marker);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        startService = findViewById(R.id.button_start_service);
        stopService = findViewById(R.id.button_stop_service);
        anchorRL = findViewById(R.id.RL_anchor);

        branchPresenter = new BranchPresenter(this);
        branchPresenter.getBranches();
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlertToUser();
        }
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!GpsProviderHelper.needPermission(MapActivity.this)) {
                    ActivityCompat.startForegroundService(MapActivity.this, new Intent(MapActivity.this, LocationService.class));

                } else {
                    requestServicePermission();
                }
            }
        });
        stopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(MapActivity.this, LocationService.class));
            }
        });


        broadCastReceiver = new networkCheck();
        intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

        registerReceiver(broadCastReceiver, intentFilter);
        getFireBaseToken();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        showBranchDialog(marker);
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.userCurrentLocation = location;
        setCurrentLocation();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (!GpsProviderHelper.needPermission(this)) {
            setUpMap();
        } else {
            requestPermission();
        }
    }

    @Override
    public void onStart() {

        googleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {

        googleApiClient.disconnect();
        unregisterReceiver(broadCastReceiver);
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    setUpMap();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    Toast.makeText(MapActivity.this, "permission denied", Toast.LENGTH_LONG).show();


                }
                break;
            case LOCATION_SERVICE_CODE:
                ActivityCompat.startForegroundService(this, new Intent(this, LocationService.class));
                break;
        }
    }

    @Override
    public void getBranch(List<Branch> branchesList) {
        for (Branch branch : branchesList) {

            branchMarker = new MarkerOptions();
            if (branch.getLat() != null && branch.getLng() != null) {
                branchLocation = new LatLng(branch.getLat(), branch.getLng());
            }
            branchLocation = new LatLng(branch.getLat(), branch.getLng());
            branchMarker.position(branchLocation);
            branchMarker.icon(BitmapDescriptorFactory.fromBitmap(branchIcon));

            branchMarker.title(branch.getAddress() + "#" + branch.getCityArabicName() + "#" + branch.getArabicName() + "#");

            mMap.addMarker(branchMarker);
        }
    }

    @Override
    public void getBranchFailure(String message) {

    }

    private void showBranchDialog(final Marker marker) {

        if (markerPopupWindow != null) {
            markerPopupWindow.dismiss();
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        mPreMarker = marker;
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(branchSelectedIcon));
        View view = getLayoutInflater().inflate(R.layout.dialog_map, null);
        markerPopupWindow = new PopupWindow(view, (width * 1), ViewGroup.LayoutParams.WRAP_CONTENT, true);
        markerPopupWindow.setOnDismissListener(() -> {
            mPreMarker.setIcon(BitmapDescriptorFactory.fromBitmap(branchIcon));
        });
        branchAddress = view.findViewById(R.id.TV_address);
        branchDistric = view.findViewById(R.id.TV_district);
        branchDistance = view.findViewById(R.id.TV_distance);
        goBranch = view.findViewById(R.id.LL_goBranch);
        mapDialog = view.findViewById(R.id.LL_container);

        branchData = marker.getTitle().split("#");

        branchAddress.setText(branchData[0]);
        if (branchData.length > 1)
            branchDistric.setText(branchData[1]);

        if ( marker==null&&marker.getPosition()==null) {
            return;

        }
        double distance = distance(marker.getPosition().latitude, marker.getPosition().longitude, currentUserMarker.getPosition().latitude, currentUserMarker.getPosition().longitude);
        branchDistance.setText(String.format("%.2f", distance) + "KM");
        goBranch.setOnClickListener(v -> {

            String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", marker.getPosition().latitude, marker.getPosition().longitude, "TO " + currentUserMarker.getPosition().latitude, currentUserMarker.getPosition().longitude);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                try {
                    Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(unrestrictedIntent);
                } catch (ActivityNotFoundException innerEx) {

                    Toast.makeText(MapActivity.this, "error", Toast.LENGTH_SHORT).show();
                }

            }
        });
        markerPopupWindow.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, android.R.color.transparent)));

        markerPopupWindow.showAsDropDown(anchorRL);
        //   markerPopupWindow.update(0, 0, 300, 150);
    }

    public void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("enable GPS")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.open_setting),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @SuppressLint("MissingPermission")
    private void setUpMap() {
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setOnMapLoadedCallback(this);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMarkerClickListener(this);

    }

    @Override
    public void onMapLoaded() {

    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(MapActivity.this
                , new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                        , Manifest.permission.ACCESS_COARSE_LOCATION}
                , LOCATION_PERMISSION_REQUEST_CODE
        );
    }

    private void requestServicePermission() {

        ActivityCompat.requestPermissions(MapActivity.this
                , new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                        , Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.FOREGROUND_SERVICE}
                , LOCATION_SERVICE_CODE
        );
    }

    public void setCurrentLocation() {

        locationIcon = BitmapFactory.decodeResource(getResources(), R.drawable.man);

        currentUserMarker = new MarkerOptions();
        userLocation = new LatLng(userCurrentLocation.getLatitude(), userCurrentLocation.getLongitude());
        currentUserMarker.position(userLocation);
        currentUserMarker.icon(BitmapDescriptorFactory.fromBitmap(locationIcon));
        currentUserMarker.title("location");

        if (mMarkerMyLocation != null)
            mMarkerMyLocation.remove();

        mMarkerMyLocation = mMap.addMarker(currentUserMarker);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentUserMarker.getPosition(), 14));

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        fusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if (location == null) return;
            userCurrentLocation = location;
            LatLng current = new LatLng(userCurrentLocation.getLatitude(), userCurrentLocation.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(current, 17F), null);
            setCurrentLocation();
        });

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public double distance(double lat1, double lng1, double lat2, double lng2) {

        Double distanceMarker;
        double earthRadius = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        distanceMarker = earthRadius * c;
        Log.d("distanceMarker", distanceMarker.toString());
        return distanceMarker;
    }

    private void getFireBaseToken() {
        //  FirebaseApp.initializeApp(this);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }

                    // Get new Instance ID token

                    String token = task.getResult().getToken();
                    if (token != null) {

                        Log.e("token1", token);
                    }


                });

    }
}
