package com.example.myapplication.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 10/30/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */

@SuppressLint("CheckResult")
public abstract class GpsProviderHelper {


    public static final int REQUEST_ENABLE_LOCATION_SERVICE = 10;
    public static final int ERROR_GOOGLE_PLAY_SERVICE_CODE = 12;
    private GoogleApiClient googleApiClient;

    public static boolean isGPSEnabled(Context context) {
        if (needPermission(context)) return false;
        LocationManager locationManager = (LocationManager) context.getSystemService(Service.LOCATION_SERVICE);
        if (locationManager == null) return false;
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean needPermission(Context context) {
        return context == null || ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }

    public void enableGps() {
        if (getActivity() == null) return;
        if (needPermission(getActivity())) {
            onNeedPermissions();
        }
        if (googleApiClient == null) {
            setupGoogleService();
            return;
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(result1 -> {
            final Status status = result1.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    if (getActivity() == null) break;
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(getActivity(), REQUEST_ENABLE_LOCATION_SERVICE);


                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                    }
                    break;
            }
        });

    }

    public void setupGoogleService() {
        if (getActivity() == null) return;
        int statusCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity().getApplicationContext());
        if (statusCode == ConnectionResult.SUCCESS) {

            checkGoogleClient();

        } else {
            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), statusCode, ERROR_GOOGLE_PLAY_SERVICE_CODE).show();
        }
    }

    protected abstract Activity getActivity();

    private void checkGoogleClient() {
        if (getActivity() == null) return;
        if (needPermission(getActivity())) {
            onNeedPermissions();
        }
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            onApiClientConnected();
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(connectionResult -> {
                        onErrorMessage(connectionResult.getErrorMessage());
                    })
                    .build();
            googleApiClient.connect();
        } else {
            onApiClientConnected();
        }

    }

    protected void onErrorMessage(String error) {
    }

    protected void onApiClientConnected() {
    }

    protected void onNeedPermissions() {


    }


}
